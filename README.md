# enmore-listener

enmore-listener is a job listener and executioner written in Python3 that listens to the BSV blockchain for broadcasted jobs and executes them. Currently this has been tested on Ubuntu 20.04 and I imagine it would run fine on other distributions of Linux, but Windows and macOS will need some minor adjustments. If you have problems and can't troubleshoot them yourself, join https://t.me/enmore.

## Installation

This package makes use of a couple of Python3 libraries that you will need to install, namely `cpuinfo` and `distro`. You can install these by running.

```bash
pip install distro cpuinfo
```

You will also need to have AutoDock vina installed and accessible in your $PATH. This is trivial on Ubuntu as you can install it by running:

```bash
sudo apt update
sudo apt install autodock-vina
```

## Usage

Once the necessary dependencies are installed you can start listening and performing jobs simply by running.

```bash
python3 start.py
```

This will take you through a one time setup wizard and begin to listen to jobs and ping the network to make it known that you are still available.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
See LICENSE file.