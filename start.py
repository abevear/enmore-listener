import argparse 
import os
import sys
import subprocess
import re
import readline
import requests
import platform
import json
import distro
import cpuinfo
import sseclient
import time
from configparser import ConfigParser

BITSOCKET_URL = 'https://txo.bitsocket.network/s/ewogICJ2IjogMywKICAicSI6IHsKICAgICJmaW5kIjogewogICAgICAib3V0LnMyIjogIjEzOTNRTEw2TXU5UEdvTmh3RHd0elRzQnM5bmZ5SG14aG0iCiAgICB9CiAgfSwKICAiciI6IHsKICAgICJmIjogIlsuW10gIHwgeyB0eGlkOiAudHguaCwgam9iOiAub3V0WzBdLnMzIHwgZnJvbWpzb259XSIKICB9Cn0='
ENMORE_API = "https://api.enmore.io"
#ENMORE_API = "http://localhost:3000"


# Warning! Here ye be some mighty contradicatory code. I'm not sure if even fully works yet,
# as I've been gluing it together over a period of a few months inbetween working and Bitping.
# Please join https://t.me/enmore if you have questions or would like to contribute.
#
# Next major release I expect will be a refactor, putting much of these into segregated functions
# across multiple files to make it less of a monolith. 

def Help():
    parser = argparse.ArgumentParser(description='Enmore Listener and Job Executioner.\nConfiguration will be loaded from your config.ini file unless overruled by parameters.\n\nExecute with no parameters to configure for the first time.', formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-c','--cpus', action='store', type=int, default=None, help="Number of CPUs ({} available)".format(os.cpu_count()))
    parser.add_argument('-k','--credential', action='store', type=str, default=None, help="Credentials file location.")
    parser.add_argument('-i','--hostname', action='store', type=str, default=None, help="Your hostname")
    parser.add_argument('-a','--api', action='store', type=str, default=None, help="Enmore API.")
    args = parser.parse_args()

    argumentsDictionary = {
        "cpus": args.cpus,
        "credential": args.credential,
        "hostname": args.hostname,
        "api": args.api
    }

    return argumentsDictionary

def askVina():
    vina_location = str(subprocess.check_output(['which','vina']).decode(sys.stdout.encoding).strip())

    if len(vina_location) >= 1:
        detected = " [auto-detected]"
    else:
        detected = ''
        
    readline.set_startup_hook(lambda: readline.insert_text(vina_location))
    try:
        locationAnswer = input(f"Location of vina binary{detected}: ")  # or raw_input in Python 2
    finally:
        readline.set_startup_hook()

    return locationAnswer

def askPaymail():
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    invalid = True

    while invalid:
        answer = input("Enter your paymail: ")  # or raw_input in Python 2
        if(re.search(regex, answer)):
            invalid = False
        else:
            print('Invalid email. ')
            invalid = True
     
    return answer
    
def askPrice():
    invalid = True

    while invalid:
        try:
            answer = float(str(input("Enter your asking price for jobs (USD): ")))  # or raw_input in Python 2

            invalid = False
        except:
            print('Please enter a number.')
     
    return answer

def askHostname():
    readline.set_startup_hook(lambda: readline.insert_text(platform.node()))
    try:
        answer = input("Name your host: ")  # or raw_input in Python 2
    finally:
        readline.set_startup_hook()
    return answer

def askCPUs():
    try:
        cpuAnswer = int(str(input(f'Number of CPUs to use (default: {os.cpu_count()}) [1-{os.cpu_count()}]: ')))
    except:
        print('Please enter a number.')
        cpuAnswer = int(str(input(f'Number of CPUs to use (default: {os.cpu_count()}) [1-{os.cpu_count()}]: ')))
    if cpuAnswer > os.cpu_count():
        print('CPUs higher than CPUs available.')
    return cpuAnswer



def InitalSetup():
    hostname = ''
    while len(hostname) < 1:
        hostname = askHostname()

    paymail = ''
    while len(paymail) < 1:
        paymail = askPaymail()

    askingPrice = askPrice()

    vinaLocation = ''
    while os.path.exists(vinaLocation) == False:
        vinaLocation = askVina()

    noCPUs = 999
    while noCPUs > os.cpu_count():
        noCPUs = askCPUs()

    WriteConfiguration(hostname, paymail, vinaLocation, noCPUs, askingPrice)

def Initalisation():
    arguments = Help()

    if os.path.exists('config.ini') == False:
        print('No config found. Starting wizard.')
        InitalSetup()
        return ReadConfiguration('config.ini', arguments)
    else:
        print('Configuration found')
        return ReadConfiguration('config.ini', arguments)

def WriteConfiguration(hostname, paymail, loc, cpu, askingPrice):
    config = ConfigParser()
    config.read('config.ini')
    if config.has_section('configuration'):
        print ('Config exists')
    else:
        config.add_section('configuration')
        config.set('configuration', 'api', 'http://api.enmore.io')
        config.set('configuration', 'paymail', paymail)
        config.set('configuration', 'asking_price', str(askingPrice))
        config.set('configuration', 'hostname', hostname)
        config.set('configuration', 'binary', loc)
        config.set('configuration', 'cpus', str(cpu))
        with open('config.ini', 'w') as f: config.write(f)
        print('Written config to file.')

def ReadConfiguration(cf, paramsDict):
    config = ConfigParser()
    config.read(cf)

    # Overule any config.ini values if prespecified in the arguments
    if paramsDict['cpus'] is None:
        paramsDict['cpus'] = config['configuration']['cpus']

    if paramsDict['api'] is None:
        paramsDict['api'] = config['configuration']['api']
    
    paramsDict['hostname'] = config['configuration']['hostname']
    paramsDict['paymail'] = config['configuration']['paymail']
    paramsDict['asking_price'] = config['configuration']['asking_price']
    paramsDict['vina'] = config['configuration']['binary']

    return paramsDict

def DockLigand(vina, ligand, receptor, parameters, cpu, id):
    executionList=[str(vina), '--ligand', str(ligand), '--receptor', str(receptor), '--cpu', str(cpu)]+parameters
    print(executionList)

    if os.path.exists('./logs/'):
            logfile = open("./logs/"+id+'.log', 'a')
            proc=subprocess.Popen(executionList, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

            output = ""
            while True:
                out = proc.stdout.read(1)
                # print(out)
                if out == '' and proc.poll() != None:
                    break
                if out != '':
                    sys.stdout.write(out)
                    output=output+out
                    logfile.write(out)
                    sys.stdout.flush()
            return
    else:
        os.mkdir('./logs/')
        logfile = open("./logs/"+id+'.log', 'a')
        proc=subprocess.Popen(executionList, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

        output = ""
        while True:
            out = proc.stdout.read(1)
            # print(out)
            if out == '' and proc.poll() != None:
                break
            if out != '':
                sys.stdout.write(out)
                output=output+out
                logfile.write(out)
                sys.stdout.flush()
        return


def bitsocketQuery(function):
    print("Listening to bitsocket...")
    r = requests.get(BITSOCKET_URL, stream=True)

    client = sseclient.SSEClient(r)
    
    for evt in client.events():
        if evt.id == 'undefined':
            pass
        else:
            function(json.loads(evt.data))

def job_event(data):
    if data['data'] != []:
        return(data['data'])

def downloadFile(location, id, ftype):
    print(f'Downloading {ftype}')
    r = requests.get(location+'/jobs/'+id+'/'+ftype)
    # print(r.content)
    try:
        if os.path.exists('proteins/'):
            output = f'proteins/{id}.{ftype}'
            content = r.content.decode(encoding='UTF-8')
            open(output, 'w+').write(content)

            return os.path.abspath(output)
        else:
            os.mkdir('proteins/')
            output = f'proteins/{id}.{ftype}'
            content = r.content.decode(encoding='UTF-8')
            open(output, 'w+').write(content)

            return os.path.abspath(output)
    except Exception as e:
        print("Error")
        print(e)
    return

def bitsocket_handler(j):
    if job_event(j) is not None:
        myDict = job_event(j)
        if float(myDict[0]['job']['jobs']['bid']) >= float(globalAskingPrice):
            try:
                print("Found new job: "+myDict[0]['txid'])

                jobId = str(myDict[0]['job']['jobs']['id'])

                if float(myDict[0]['job']['jobs']['bid']) >= float(globalAskingPrice):
                    print("Job met minimum ask. Starting docking...")
                try:
                    proteinPath = downloadFile(apiLocation, myDict[0]['job']['jobs']['id'], 'protein')
                except Exception as e:
                    print('Downloading protein failed.')
                    print(e)
                    pass

                try:
                    ligandPath = downloadFile(apiLocation, myDict[0]['job']['jobs']['id'], 'ligand')
                except Exception as e:
                    print('Downloading ligand failed.')
                    print(e)
                    pass

                print("Preparing parameters...")
                try:
                    parameters = str(myDict[0]['job']['jobs']['params']).split()
                    fullString = ' '.join(parameters)
                    if ".." in fullString or "\\" in fullString or "{" in fullString or "}" in fullString:
                        raise Exception('Illegal characters found in params.')
                except Exception as e:
                    print('Failed to prepare parameters. Nefarious request?')
                    print(e)
                    return

                with open('lastToken', 'r') as file:
                    bearertoken = file.read().replace('\n', '')

                with open('latestPeerId', 'r') as file:
                    latestPeerId = file.read().replace('\n', '')


                headers = {"Authorization": f"Bearer {bearertoken}"}
                
                attemptingRequest = requests.post(f'{ENMORE_API}/jobs/{jobId}/peer', headers=headers)

                if attemptingRequest.text == "Cannot retry job if already failed.":
                    return

                # Run job
                DockLigand('/usr/bin/vina', ligandPath, proteinPath, parameters, globalCPUs, jobId)
                print('Enmore taking the wheel again...')

                #read stdoutput and post it

                print('Uploading job logs...')
                try:
                    with open('./logs/'+jobId+'.log', 'r') as file:
                        outputData = file.read()
                        postStdOut = requests.post(f'{ENMORE_API}/jobs/{jobId}/{latestPeerId}/output', data={'output': outputData}, headers=headers)
                        print(postStdOut)
                except Exception as e:
                    print("Log upload failed.")
                    print(e)
                    return

                print('Uploading job result...')
                try:
                    with open(f'proteins/{jobId}.ligand_out.pdbqt', 'rb') as f:
                        data = {'result': f}
                        postCompletedFile = requests.post(f'{ENMORE_API}/jobs/{jobId}/{latestPeerId}/result', headers=headers, files=data)
                        print(postCompletedFile)
                except Exception as e:
                    print("Result file upload failed.")
                    requests.delete(f'{ENMORE_API}/jobs/{jobId}/peer', headers=headers)  
                    print(e)
                    return


                print('Updating job status.')
                if os.path.exists(f'proteins/{jobId}.ligand_out.pdbqt'):
                    if os.path.getsize(f'proteins/{jobId}.ligand_out.pdbqt') > 0:
                        requests.put(f'{ENMORE_API}/jobs/{jobId}/peer', headers=headers)
                        print('Success.')
                    else:
                        requests.delete(f'{ENMORE_API}/jobs/{jobId}/peer', headers=headers)   
                        print('Job completed unsuccessfully. Not necessarily your fault!')
                else:
                    requests.delete(f'{ENMORE_API}/jobs/{jobId}/peer', headers=headers)
                    print('Job completed unsuccessfully. Not necessarily your fault!')

            except Exception as e:
                print("Error occured.")
                print(e)
                pass
        else:
            print("Heard new job. Didn't meet asking price.")
            pass

from multiprocessing import Process

def socketListener():
    time.sleep(2)
    bitsocketQuery(bitsocket_handler)

def keepPeerAlive():
    nodeInformation = {
            'name': globalHostname,
            'paymail': globalPaymail,
            'nodeversion': "0.0.1",
            'sysinfo' : distro.linux_distribution()[0]+' '+distro.linux_distribution()[1],
            'cpu': cpuinfo.get_cpu_info()['brand'].replace('(R)','').replace('(TM)','').split(' @ ',1)[0],
            'ask': globalAskingPrice
    }
    response = requests.post(f'{ENMORE_API}/peers', data=nodeInformation)
    print("Joined the network with peerid: "+json.loads(response.text)['savedPeer']['id'])

    with open('latestPeerId', 'w') as latestPeerId:
        latestPeerId.write(json.loads(response.text)['savedPeer']['id'])
    with open('lastToken', 'w') as tokenFile:
        tokenFile.write(json.loads(response.text)['token'])

    time.sleep(1500)
    while True:
        with open('lastToken', 'r') as file:
            bearertoken = file.read().replace('\n', '')
            headers = {"Authorization": f"Bearer {bearertoken}"}

        response = requests.put(f'{ENMORE_API}/peers', data=nodeInformation, headers=headers)


        print("Kept node alive with peerid: "+json.loads(response.text)['id'])

        time.sleep(1500)

if __name__ == '__main__':
    print(' _____                               ')
    print('|  ___|                              ')
    print('| |__ _ __  _ __ ___   ___  _ __ ___ ')
    print("""|  __| '_ \| '_ ` _ \ / _ \| '__/ _ \\""")
    print("| |__| | | | | | | | | (_) | | |  __/")
    print('\____/_| |_|_| |_| |_|\___/|_|  \___|')
    print("v0.0.1 alpha -- it's pretty shitty. @libertytxn\n")


    rules = Initalisation()
    print(rules)

    global apiLocation
    apiLocation = rules['api']

    global globalHostname
    globalHostname = rules['hostname']
    
    global globalPaymail
    globalPaymail = rules['paymail']    

    global globalAskingPrice
    globalAskingPrice = rules['asking_price']

    global globalCPUs
    globalCPUs = rules['cpus']

    p1 = Process(target=keepPeerAlive)
    p1.start()

    p2 = Process(target=socketListener)
    p2.start()

    p1.join()
    p2.join()